package com.example.tasksexample;

import android.content.Context;

/**
 * 
 */
public class AccountPrefs {
	
	private static final String PREF_FILE_NAME = "accountPrefs";
	private static final String PREF_ACCOUNT_NAME = "accountName";
	private static final String PREF_AUTH_TOKEN = "authToken";
	
	public static String getAccountName(Context context, String defValue) {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, defValue);
	}
	
	public static boolean setAccountName(Context context, String accountName) {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit().putString(PREF_ACCOUNT_NAME, accountName).commit();
	}
	
	public static String getAuthToken(Context context, String defValue) {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getString(PREF_AUTH_TOKEN, defValue);
	}
	
	public static boolean setAuthToken(Context context, String authToken) {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit().putString(PREF_AUTH_TOKEN, authToken).commit();
	}

}
