package com.example.tasksexample;

import com.google.api.client.googleapis.extensions.android.accounts.GoogleAccountManager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * 
 */
public class AccountActivity extends Activity {

	private static final String TAG = "AccountActivity";
	private static final int REQUEST_AUTHENTICATE = 0;
	public static final String EXTRA_AUTH_TOKEN_TYPE = "authtokentype";
	public static final String EXTRA_ACCOUNT_NAME = "accountname";
	public static final String EXTRA_AUTH_TOKEN = "authtoken";
	private GoogleAccountManager accountManager;
	private String authTokenType;
	
	public static void start(Activity activity, int requestCode, String accountName, String authTokenType) {
		Intent intent = new Intent(activity, AccountActivity.class);
		intent.putExtra(EXTRA_AUTH_TOKEN_TYPE, authTokenType);
		intent.putExtra(EXTRA_ACCOUNT_NAME, accountName);
		activity.startActivityForResult(intent, requestCode);
	}
	
	private AccountManagerCallback<Bundle> gotAccountCallback = new AccountManagerCallback<Bundle>() {
		public void run(AccountManagerFuture<Bundle> future) {
			try {
				Bundle bundle = future.getResult();
				if (bundle.containsKey(AccountManager.KEY_INTENT)) {
					// we got an account by name, but AccountManager says we need to Authenticate
					Intent intent = bundle.getParcelable(AccountManager.KEY_INTENT);
					intent.setFlags(intent.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivityForResult(intent, REQUEST_AUTHENTICATE);
				} else if (bundle.containsKey(AccountManager.KEY_AUTHTOKEN)) {
					// we got an account and account manager already has an auth token
					String accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
					String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
					onAuthToken(accountName, authToken);
				}
			} catch (Exception e) {
				onException(e);
			}
		}
	};
	
	private AccountManagerCallback<Bundle> chooseAccountCallback = new AccountManagerCallback<Bundle>() {
		public void run(AccountManagerFuture<Bundle> future) {
			Bundle bundle;
			try {
				bundle = future.getResult();
				String accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
				String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
				onAuthToken(accountName, authToken);
			} catch (OperationCanceledException e) {
				onCancel();
			} catch (Exception e) {
				onException(e);
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		authTokenType = getIntent().getStringExtra(EXTRA_AUTH_TOKEN_TYPE);
		String accountName = getIntent().getStringExtra(EXTRA_ACCOUNT_NAME);
		accountManager = new GoogleAccountManager(this);
		gotAccount(accountName);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_AUTHENTICATE:
			if (resultCode == RESULT_OK) {
				String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				gotAccount(accountName);
			} else {
				chooseAccount();
			}
			break;
		}
	}
	
	void gotAccount(String accountName) {
		Account account = accountManager.getAccountByName(accountName);
		if (account == null) {
			chooseAccount();
			return;
		}
		accountManager.getAccountManager().getAuthToken(
			account, 
			authTokenType,
			true, 
			gotAccountCallback,
			null
		);
	}

	private void chooseAccount() {
		accountManager.getAccountManager().getAuthTokenByFeatures(GoogleAccountManager.ACCOUNT_TYPE,
			authTokenType,
			null,
			AccountActivity.this,
			null,
			null,
			chooseAccountCallback,
			null
		);
	}
	
	void onAuthToken(String accountName, String authToken) {
		setResult(Activity.RESULT_OK);
		Intent data = new Intent();
		data.putExtra(EXTRA_ACCOUNT_NAME, accountName);
		data.putExtra(EXTRA_AUTH_TOKEN, authToken);
		setResult(RESULT_OK, data);
		finish();
	}
	
	void onCancel() {
		setResult(RESULT_CANCELED);
		finish();
	}
	
	void onException(Exception e) {
		setResult(RESULT_FIRST_USER);
		Log.e(TAG, e.getMessage(), e);
		finish();
	}
}
