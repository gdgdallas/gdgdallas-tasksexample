package com.example.tasksexample;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.services.GoogleKeyInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.model.Task;

/**
 * Sample for Tasks API on Android. It shows how to authenticate using OAuth 2.0, and get the list
 * of tasks.
 * <p>
 * To enable logging of HTTP requests/responses, change {@link #LOGGING_LEVEL} to
 * {@link Level#CONFIG} or {@link Level#ALL} and run this command:
 * </p>
 * 
 * <pre>
adb shell setprop log.tag.HttpTransport DEBUG
 * </pre>
 * 
 * @author Johan Euphrosine
 * @author Don Archer
 */
public final class TasksSample extends ListActivity {

	private static final Level LOGGING_LEVEL = Level.ALL; /** Logging level for HTTP requests/responses. */
	private static final String TAG = "TasksSample";
	// This must be the exact string, and is a special for alias OAuth 2 scope
	// "https://www.googleapis.com/auth/tasks"
	private static final String AUTH_TOKEN_TYPE = "Manage your tasks";
	private final HttpTransport transport = AndroidHttp.newCompatibleTransport();
	private final JsonFactory jsonFactory = new GsonFactory();
	GoogleCredential credential = new GoogleCredential();
	com.google.api.services.tasks.Tasks service;
	boolean received401;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ClientCredentials.errorIfNotSpecified();
		Logger.getLogger("com.google.api.client").setLevel(LOGGING_LEVEL);
		service = new com.google.api.services.tasks.Tasks.Builder(transport, jsonFactory, credential)
			.setApplicationName("Google-TasksAndroidSample/1.0")
			.setJsonHttpRequestInitializer(new GoogleKeyInitializer(ClientCredentials.KEY))
			.build();
		
		String authToken = AccountPrefs.getAuthToken(this, null);

		if(TextUtils.isEmpty(authToken)) {
			AccountActivity.start(this, 0, AccountPrefs.getAccountName(this, null), AUTH_TOKEN_TYPE);
		} else {
			credential.setAccessToken(authToken);
			new AsyncLoadTasks().execute();
		}
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK) {
			String authToken = data.getStringExtra(AccountActivity.EXTRA_AUTH_TOKEN);
			String accountName = data.getStringExtra(AccountActivity.EXTRA_ACCOUNT_NAME);
			AccountPrefs.setAccountName(this, accountName);
			AccountPrefs.setAuthToken(this, authToken);
			credential.setAccessToken(authToken);
			new AsyncLoadTasks().execute();
		} else {
			// there was an error getting an account
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_changeaccount:
			AccountPrefs.setAccountName(this, null);
			AccountActivity.start(this, 0, null, AUTH_TOKEN_TYPE);
			return true;
		case R.id.menu_logout:
			AccountManager.get(this).invalidateAuthToken(AUTH_TOKEN_TYPE, credential.getAccessToken());
			AccountPrefs.setAccountName(this, null);
			AccountPrefs.setAuthToken(this, null);
			finish();
			return true;
		}
		return false;
	}

	void handleGoogleException(IOException e) {
		Log.e(TAG, e.getMessage(), e);
		if (e instanceof GoogleJsonResponseException) {
			GoogleJsonResponseException exception = (GoogleJsonResponseException) e;
			if (exception.getStatusCode() == 401 && !received401) {
				received401 = true;
				AccountManager.get(this).invalidateAuthToken(AUTH_TOKEN_TYPE, credential.getAccessToken());
				credential.setAccessToken(null);
				AccountPrefs.setAuthToken(this, null);
				AccountActivity.start(this, 0, AccountPrefs.getAccountName(this, null), AUTH_TOKEN_TYPE);
				return;
			}
		}
		Log.e(TAG, e.getMessage(), e);
	}

	class AsyncLoadTasks extends AsyncTask<Void, Void, List<String>> {
		
		private final ProgressDialog dialog;

		AsyncLoadTasks() {
			dialog = new ProgressDialog(TasksSample.this);
		}

		@Override
		protected void onPreExecute() {
			dialog.setMessage("Loading tasks...");
			dialog.show();
		}

		@Override
		protected List<String> doInBackground(Void... arg0) {
			try {
				List<String> result = new ArrayList<String>();
				Tasks.TasksOperations.List listRequest = service.tasks().list("@default");
				listRequest.setFields("items/title");
				List<Task> tasks = listRequest.execute().getItems();
				if (tasks != null) {
					for (Task task : tasks) {
						result.add(task.getTitle());
					}
				} else {
					result.add("No tasks.");
				}
				return result;
			} catch (IOException e) {
				handleGoogleException(e);
				return Collections.singletonList(e.getMessage());
			} finally {
				received401 = false;
			}
		}

		@Override
		protected void onPostExecute(List<String> result) {
			dialog.dismiss();
			setListAdapter(new ArrayAdapter<String>(TasksSample.this, android.R.layout.simple_list_item_1, result));
		}
	}
}